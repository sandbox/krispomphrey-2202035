<?php
/**
 * @file
 * JustGiving Helper Class.
 */

libraries_load('justgiving');

class JustGiving {
  public $settings = array();
  public $forms = array();
  public $client;
  public $session;

  /**
   * Implements a custom debug.
   */
  public function debug($data, $output = TRUE) {
    if ($output) {
      debug($data);
    }
  }

  /**
   * Constructor Function.
   *
   * Function to build the session and set up the PHP SDK for use
   */
  public function __construct() {
    // Reference session varibales in our class
    $this->session = &$_SESSION['jg'];

    if (!isset($_SESSION)) {
      drupal_session_start();
    }
    // debug($this->session);
    $this->getSettings();
    $this->client = new JustGivingClient($this->settings['environment'], $this->settings['api-key'], 1);
  }

  /**
   * Destructor Function.
   *
   * In case this is fired reassign everything back into session for use later.
   */
  public function __destruct() {
    // debug($this->session);
    // Assign session variable back into $_SESSION global.
    $_SESSION['jg'] = $this->session;
    // debug($_SESSION);
  }

  /**
   * Settings Function.
   *
   * Pull the variables once from database and assign to class variables.
   */
  public function getSettings() {
    $this->settings = array(
      'charity-id' => variable_get('justgiving_charity_id'),
      'api-key' => variable_get('justgiving_api_key'),
      'environment' => variable_get('justgiving_environment'),
      'suggestions' => variable_get('justgiving_suggestions_choice')
    );
    if($this->settings['suggestions'] == 'custom'){
      $this->settings['suggestions-custom'] = variable_get('justgiving_suggestions_choice_custom');
    }
    $this->forms = array(
      'account' => variable_get('justgiving_node_account_form'),
      'page' => variable_get('justgiving_node_page_form'),
    );
  }

  /**
   * Custom JustGiving function for creating accounts.
   *
   * @return: Returns the response, or an error message.
   */
  public function accounts($type = 'list', $data = array()) {
    if ($this->settings['api-key'] && !empty($data)) {
      switch ($type) {
        case 'create':
          if (!$this->client->Account->IsEmailRegistered($data['email'])) {
            return $this->client->Account->Create($data);
          } else {
            return array(
              'result' => FALSE,
              'message' => 'Email Address is already taken',
            );
          }
          break;

        case 'list':
          // TODO: Will list pages attached to said account.
          break;
      }
    } else {
      return array(
        'result' => FALSE,
        'message' => 'Cannot complete request',
      );
    }
  }

  /**
   * Custom JustGiving function for creating pages.
   *
   * @return: Returns either the response or an error message.
   */
  public function pages($type = 'list', $auth = array(), $data = array()) {
    // debug($data);
    if ($this->settings['api-key'] && !empty($data)) {
      switch ($type) {
        case 'create':
          // Re-invoke the client with Auth details to create a page.
          $this->client = new JustGivingClient($this->settings['environment'], $this->settings['api-key'], 1, $auth['username'], $auth['password']);
          if (isset($data['pageShortName']) && !$this->client->Page->IsShortNameRegistered($data['pageShortName'])) {
            $page = new RegisterPageRequest();
            foreach ($data as $key => $value) {
              if (is_array($value)) {
                foreach ($value as $keysub => $valuesub) {
                  $page->$key->$keysub = $valuesub;
                }
              } else {
                $page->$key = $value;
              }
            }
            // debug($page);
            $response = $this->client->Page->Create($page);
            // debug($response);
            return $response;
          } else {
            return array(
              'result' => FALSE,
              'message' => 'Shortname taken already',
            );
          }
          break;

        case 'suggest':
          // $this->debug($data[0]);
          return $this->client->Page->SuggestPageShortNames($data[0]);
          break;
      }
    } else {
      return array(
        'result' => FALSE,
        'message' => 'Cannot complete request',
      );
    }
  }
}
